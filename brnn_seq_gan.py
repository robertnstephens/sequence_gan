#!/usr/bin/env python3

# Generative Adversarial Networks (GAN) example in PyTorch.
# See related blog post at https://medium.com/@devnag/generative-adversarial-networks-gans-in-50-lines-of-code-pytorch-e81b79659e3f#.sch4xgsa9
import sys, os
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from sklearn.preprocessing import MinMaxScaler

sys.path.append("./py/")
import Wraph5py

kmer_len = 3
num_classes = pow(4, kmer_len)+1
features = 12
time_steps = 1
batch_size = 1000

# Data params
data_mean = 4
data_stddev = 1.25

# Model params
g_input_size = features # Random noise dimension coming into generator, per output vector
g_hidden_size = 100   # Generator complexity
g_output_size = features # size of generated output vector
#d_input_size = 100   # Minibatch size - cardinality of distributions
d_input_size = features
d_hidden_size = 128   # Discriminator complexity
d_output_size = num_classes # was single dimension for 'real' vs. 'fake'
minibatch_size = d_input_size

d_learning_rate = 1e-3
g_learning_rate = 1e-3
optim_betas = (0.9, 0.999)
num_epochs = 500000
d_steps = 1  # 'k' steps in the original GAN paper. Can put the discriminator on higher training freq than generator
g_steps = 1

import logging
logging.basicConfig(filename="training.log",level=logging.INFO)


def get_acc(results, expected):
    prediction = torch.max(softmax(results), 1)[1]
    correct_prediction = (prediction.data == expected.data)
    accuracy = correct_prediction.float().mean()
    return accuracy

def run_gan(files):
   
    use_sim = False
    test_mode = False

    for file_in in files:
        if use_sim:
            XY = np.loadtxt(file_in, delimiter=',', dtype=np.float32)
            all_X = XY[:, 0:features]
            all_Y_num = XY[:, [-1]].astype(np.int64)
        else: 
            print("processing file:", file_in)
            all_X, all_Y_num, all_labels, all_kmers = Wraph5py.get_training_data(file_in, kmer_len=kmer_len)
        
        batches = [(i,i+batch_size) for i in range(0, len(all_X), batch_size)]

        for b, batch in enumerate(batches):
            #print("processing batch:", b, "of", len(batches)) 

            # 1. Train disc_rnn on real+fake
            disc_rnn.zero_grad() # clears the gradients of all optimized variables
    
            #  1A: Train disc_rnn on real
            X = all_X[batch[0]:batch[1]]
            Y = all_Y_num[batch[0]:batch[1]]

            scaler = MinMaxScaler(feature_range=(0, 1))
            X = scaler.fit_transform(X)

            data_len = len(X)
    
            X = X.reshape(time_steps, -1, features)
            d_real_data = Variable(torch.from_numpy(X.astype(np.float32))) # (data_len, features)
            Y = Y.reshape(-1,) 
            Y = Variable(torch.from_numpy(Y.astype(np.int64)))
            
            for rep in range(num_classes-1):   
                #print("disc train real:", rep+1, "of", num_classes-1)
                output_seq, _ = disc_rnn(d_real_data)
                d_real_decision = output_seq[-1]
                d_real_error = criterion(d_real_decision, Y)  
                d_real_error.backward() # compute/store gradients, but don't change params
                d_real_acc = get_acc(d_real_decision, Y)
            
            #  1B: Train disc_rnn on fake
            d_gen_input = Variable(gi_sampler(data_len, features))
            d_fake_data = G(d_gen_input).detach()  # detach to avoid training G on these labels
            d_fake_data = d_fake_data.view(time_steps, -1, features) 
            
            output_seq, _ = disc_rnn(d_fake_data)
            d_fake_decision = output_seq[-1]
            XXX = Variable(torch.LongTensor([num_classes-1 for _ in range(data_len)]))
            d_fake_error = criterion(d_fake_decision, XXX)  
            d_fake_error.backward()
            d_fake_acc = get_acc(d_fake_decision, XXX)

            if test_mode:
                print(epoch, d_real_acc, d_real_error.data[0], d_fake_acc, d_fake_error.data[0])
                sys.stdout.flush()
                d_optimizer.step()     # Only optimizes D's parameters; changes based on stored gradients from backward()
                continue 

            d_optimizer.step()     # Only optimizes D's parameters; changes based on stored gradients from backward()

            # 2. Train G on D's response (but DO NOT train D on these labels)
            G.zero_grad()
    
            gen_input = Variable(gi_sampler(data_len, features))
            g_fake_data = G(gen_input)
            g_fake_data = g_fake_data.view(time_steps, -1, features) 
    
            output_seq, _ = disc_rnn(g_fake_data)
            dg_fake_decision = output_seq[-1]
            
            g_error = criterion(dg_fake_decision, Y)  # we want to fool, so pretend it's all genuine
            g_error.backward()
           
            g_fake_acc = get_acc(dg_fake_decision, Y)

            g_optimizer.step()  # Only optimizes G's parameters

            '''
            print("E: %s ACC: %s DR: %s DF: %s G: %s (Real: %s, Fake: %s) " % (epoch,
                                                             accuracy,
                                                             extract(d_real_error)[0],
                                                             extract(d_fake_error)[0],
                                                             extract(g_error)[0],
                                                             stats(extract(d_real_data)),
                                                             stats(extract(d_fake_data))))
            '''        
            if test_mode: 
                continue 
            
            out = '\nE: {}, DR_acc: {}, DF_acc: {}, GF_acc: {}, DR_loss: {}, DF_loss: {}, GF_loss: {}\n'.format(epoch, d_real_acc, d_fake_acc, g_fake_acc, 
    																											d_real_error.data[0], d_fake_error.data[0], g_error.data[0])
            print(out)
            logging.info(out)
 


#num_classes output_size
class BiRNN(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers, output_size):
        super(BiRNN, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.lstm = nn.LSTM(input_size, hidden_size, num_layers, batch_first=False, bidirectional=True)
        self.fc = nn.Linear(hidden_size*2, output_size)  # 2 for bidirection 

    def forward(self, x):
        # Set initial states
        h0 = Variable(torch.zeros(self.num_layers*2, x.size(0), self.hidden_size)) # 2 for bidirection 
        c0 = Variable(torch.zeros(self.num_layers*2, x.size(0), self.hidden_size))

        # Forward propagate RNN
        out, _ = self.lstm(x, (h0, c0))
        return self.fc(out), None


# ##### DATA: Target data and generator input data

def get_generator_input_sampler():
    return lambda m, n: torch.rand(m, n)  # Uniform-dist data into generator, _NOT_ Gaussian

# ##### MODELS: Generator model and discriminator model

class Generator(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(Generator, self).__init__()
        self.map1 = nn.Linear(input_size, hidden_size)
        self.map2 = nn.Linear(hidden_size, hidden_size)
        self.map3 = nn.Linear(hidden_size, output_size)

    def forward(self, x):
        x = F.elu(self.map1(x))
        x = F.sigmoid(self.map2(x))
        return self.map3(x)

def extract(v):
    return v.data.storage().tolist()

def stats(d):
    return [np.mean(d), np.std(d)]

def decorate_with_diffs(data, exponent):
    mean = torch.mean(data.data, 1, keepdim=True)
    mean_broadcast = torch.mul(torch.ones(data.size()), mean.tolist()[0][0])
    diffs = torch.pow(data - Variable(mean_broadcast), exponent)
    return torch.cat([data, diffs], 1)

if __name__ == "__main__":

    gi_sampler = get_generator_input_sampler()
    G = Generator(input_size=g_input_size, hidden_size=g_hidden_size, output_size=g_output_size)
    disc_rnn = BiRNN(input_size=features, hidden_size=d_hidden_size, num_layers=3, output_size=d_output_size)
    #disc_rnn = torch.nn.LSTM(input_size=features, hidden_size=num_classes, num_layers=3)

    #criterion = nn.BCELoss()  # Binary cross entropy: http://pytorch.org/docs/nn.html#bceloss
    criterion = torch.nn.CrossEntropyLoss() # loss AKA criterion
    
    d_optimizer = optim.Adam(disc_rnn.parameters(), lr=d_learning_rate, betas=optim_betas)
    #d_optimizer = torch.optim.SGD(disc_rnn.parameters(), lr=d_learning_rate)
    g_optimizer = optim.Adam(G.parameters(), lr=g_learning_rate, betas=optim_betas)
    
    softmax = torch.nn.Softmax()
    
    path = sys.argv[1]
    files = os.listdir(path)
    files = [path+f for f in files]

    for epoch in range(num_epochs):
        run_gan(files)   
