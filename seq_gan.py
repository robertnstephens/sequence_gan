#!/usr/bin/env python3

# Generative Adversarial Networks (GAN) example in PyTorch.
# See related blog post at https://medium.com/@devnag/generative-adversarial-networks-gans-in-50-lines-of-code-pytorch-e81b79659e3f#.sch4xgsa9
import sys, os
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from sklearn.preprocessing import MinMaxScaler


sys.path.append("./py/")
#import Wraph5py, PostUtils, SeqUtils, KLSTM
import Wraph5py


kmer_len = 4
features = 12



# Data params
data_mean = 4
data_stddev = 1.25

# Model params
g_input_size = features # Random noise dimension coming into generator, per output vector
g_hidden_size = 100   # Generator complexity
g_output_size = features # size of generated output vector
#d_input_size = 100   # Minibatch size - cardinality of distributions
d_input_size = features
d_hidden_size = 100   # Discriminator complexity
d_output_size = 1    # Single dimension for 'real' vs. 'fake'
minibatch_size = d_input_size

d_learning_rate = 2e-4  # 2e-4
g_learning_rate = 2e-4
optim_betas = (0.9, 0.999)
num_epochs = 4000
print_interval = 1
d_steps = 1  # 'k' steps in the original GAN paper. Can put the discriminator on higher training freq than generator
g_steps = 1


print("Using data [%s]" % (name))

# ##### DATA: Target data and generator input data

def get_generator_input_sampler():
    return lambda m, n: torch.rand(m, n)  # Uniform-dist data into generator, _NOT_ Gaussian

# ##### MODELS: Generator model and discriminator model

class Generator(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(Generator, self).__init__()
        self.map1 = nn.Linear(input_size, hidden_size)
        self.map2 = nn.Linear(hidden_size, hidden_size)
        self.map3 = nn.Linear(hidden_size, output_size)

    def forward(self, x):
        x = F.elu(self.map1(x))
        x = F.sigmoid(self.map2(x))
        return self.map3(x)

class Discriminator(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(Discriminator, self).__init__()
        self.map1 = nn.Linear(input_size, hidden_size)
        self.map2 = nn.Linear(hidden_size, hidden_size)
        self.map3 = nn.Linear(hidden_size, output_size)

    def forward(self, x):
        x = F.elu(self.map1(x))
        x = F.elu(self.map2(x))
        return F.sigmoid(self.map3(x))

def extract(v):
    return v.data.storage().tolist()

def stats(d):
    return [np.mean(d), np.std(d)]

def decorate_with_diffs(data, exponent):
    mean = torch.mean(data.data, 1, keepdim=True)
    mean_broadcast = torch.mul(torch.ones(data.size()), mean.tolist()[0][0])
    diffs = torch.pow(data - Variable(mean_broadcast), exponent)
    return torch.cat([data, diffs], 1)

gi_sampler = get_generator_input_sampler()
G = Generator(input_size=g_input_size, hidden_size=g_hidden_size, output_size=g_output_size)
D = Discriminator(input_size=d_input_size, hidden_size=d_hidden_size, output_size=d_output_size)
criterion = nn.BCELoss()  # Binary cross entropy: http://pytorch.org/docs/nn.html#bceloss
d_optimizer = optim.Adam(D.parameters(), lr=d_learning_rate, betas=optim_betas)
g_optimizer = optim.Adam(G.parameters(), lr=g_learning_rate, betas=optim_betas)


path = sys.argv[1]
#path = args.data
files = os.listdir(path)
files = [path+f for f in files]

for epoch in range(num_epochs):
    for file_in in files:
        # 1. Train D on real+fake
        D.zero_grad()

        #  1A: Train D on real
        X, Y_num, labels, kmers = Wraph5py.get_training_data(file_in, kmer_len=kmer_len)


        scaler = MinMaxScaler(feature_range=(0, 1))
        X = scaler.fit_transform(X)


        data_len = len(X)

        d_real_data = Variable(torch.from_numpy(X.astype(np.float32))) # (data_len, features)

        d_real_decision = D(d_real_data) 
        d_real_error = criterion(d_real_decision, Variable(torch.ones(data_len,1)))  # ones = true
        d_real_error.backward() # compute/store gradients, but don't change params

        #  1B: Train D on fake
        d_gen_input = Variable(gi_sampler(data_len, features))
        d_fake_data = G(d_gen_input).detach()  # detach to avoid training G on these labels
        
        d_fake_decision = D(d_fake_data)
        
        d_fake_error = criterion(d_fake_decision, Variable(torch.zeros(data_len,1)))  # zeros = fake
        d_fake_error.backward()
        d_optimizer.step()     # Only optimizes D's parameters; changes based on stored gradients from backward()

        # 2. Train G on D's response (but DO NOT train D on these labels)
        G.zero_grad()
        
        gen_input = Variable(gi_sampler(data_len, features))
        g_fake_data = G(gen_input)
        dg_fake_decision = D(g_fake_data)
        g_error = criterion(dg_fake_decision, Variable(torch.ones(data_len,1)))  # we want to fool, so pretend it's all genuine

        g_error.backward()
        g_optimizer.step()  # Only optimizes G's parameters

    if epoch % print_interval == 0:
        print("E: %s DR: %s DF: %s G: %s (Real: %s, Fake: %s) " % (epoch,
                                                            extract(d_real_error)[0],
                                                            extract(d_fake_error)[0],
                                                            extract(g_error)[0],
                                                            stats(extract(d_real_data)),
                                                            stats(extract(d_fake_data))))
        sys.stdout.flush()

