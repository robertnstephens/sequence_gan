#! /usr/bin/env python3

import sys
import numpy as np


_KMER_LEN = 3
_BASES = ["A", "C", "G", "T"]


def gen_kmers():
    kmers = []
    for i in range( pow(len(_BASES),_KMER_LEN) ):
        mask = 0x3
        val = i

        next_kmer = ["X" for _ in range(_KMER_LEN)]
        for j in range(_KMER_LEN):
            index = val & mask
            next_kmer[_KMER_LEN-1-j] = _BASES[index]
            val >>= 2

        kmers.append(''.join(next_kmer))

    bad_kmer = ''.join(["X" for _ in range(_KMER_LEN)])
    kmers.append(bad_kmer)
    return kmers


def gen_data():

    kmers = gen_kmers()
    
    kmer_dict = {}
    for i, kmer in enumerate(kmers):
        kmer_dict[kmer] = i
    
    
    num_classes = pow(len(_BASES), _KMER_LEN)
    data_len = 1000
    
    means = np.linspace(50, 80, num_classes)
    dwells = np.linspace(0.1, 0.5, num_classes)
    
    noise = np.random.uniform(-1, 1, data_len)
    d_noise = np.random.uniform(-0.01, 0.01, data_len)
    
    cur_mer = "A" * _KMER_LEN
    
    seq_out = "" 
    for p in range(data_len):
        
        next_base = np.random.randint(0, len(_BASES))
       
        seq_out += _BASES[next_base]
        
        next_mer = cur_mer[1:] + _BASES[next_base]
        mi = kmer_dict[next_mer]
        
        y = [str(0) for _ in range(num_classes)]    
        y[mi] = str(1)
    
        out = "" 
        for i in range(6):
            d = i+1
            out += str(means[mi]+noise[mi] + 10.0/d) + ", " + str(dwells[mi]+d_noise[mi] + 0.1/d) + ", "

        #out += ", " 
        #out += ", ".join(y) 
        out += str(mi)
    
        print(out)
    
        cur_mer = next_mer

    print(seq_out)

def data_to_seq(in_file):

    kmers = gen_kmers()

    kmer_dict = {}
    for i, kmer in enumerate(kmers):
        kmer_dict[i] = kmer

    xy = np.loadtxt(sys.argv[1], delimiter=',')
    
    if len(xy.shape) > 1: 
        nums = xy[:,-1]
    else: 
        nums = xy

    kmer_seq = []
    for n in nums:
        kmer_seq.append(kmer_dict[int(n)])

    seq = kmer_seq[0]
    for k in range(1, len(kmer_seq)):
        seq += kmer_seq[k][-1] 

    print(seq)

if __name__ == "__main__":

    if len(sys.argv) == 1:
        gen_data()
    else:
        data_to_seq(sys.argv[1])

